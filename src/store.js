import Vue from 'vue'
import Vuex from 'vuex'
import localforage from 'localforage'
import * as Diff from 'diff'
import baselineJson from '@/assets/baseline'

Vue.use(Vuex)

async function bootstrapDb () {
  // localforage.clear()
  localforage.config({
    driver: localforage.WEBSQL,
    name: 'pactly-analytics-tagger',
    version: 1.0,
    size: 4980736,
    storeName: 'data',
    description: ''
  })
  const baseline = await localforage.getItem('baseline')
  const baselineMap = await localforage.getItem('baselineMap')
  const variations = await localforage.getItem('variations')
  const categories = await localforage.getItem('categories')

  return { baseline, baselineMap, variations, categories }
}

export default new Vuex.Store({
  state: {
    baseline: [],
    baselineMap: [],
    variations: [],
    categories: {},
    filter: null,
    path: '',
    diffField: 'clean_text'
  },
  mutations: {
    bootstrap (state, { baseline, baselineMap, variations, categories }) {
      Vue.set(state, 'baseline', baseline)
      Vue.set(state, 'baselineMap', baselineMap)
      Vue.set(state, 'variations', variations)
      Vue.set(state, 'categories', categories || {})
    },
    setBaseline (state, { baseline, baselineMap }) {
      Vue.set(state, 'baseline', baseline)
      Vue.set(state, 'baselineMap', baselineMap)
    },
    setVariations (state, { variations }) {
      Vue.set(state, 'variations', variations)
    },
    setCategories (state, { categories }) {
      Vue.set(state, 'categories', categories)
    },
    clear (state) {
      Vue.set(state, 'baseline', [])
      Vue.set(state, 'baselineMap', [])
      Vue.set(state, 'variations', [])
    },
    setFilter (state, { index }) {
      Vue.set(state, 'filter', index)
    },
    removeFilter (state) {
      Vue.set(state, 'filter', null)
    },
    addCategory (state, { category }) {
      let categories
      const clauseCategories = state.categories[state.filter]
      if (typeof clauseCategories !== 'undefined') {
        categories = clauseCategories.slice()
        category.key = clauseCategories.length + 1
        categories.push(category)
      } else {
        category.key = 1
        categories = [category]
      }
      Vue.set(state.categories, state.filter, categories)
    },
    editCategory (state, { category }) {
      const clauseCategories = state.categories[state.filter]
      const index = clauseCategories.findIndex(cat => cat.key === category.key)
      Vue.set(state.categories[state.filter], index, category)
    },
    setTag (state, { id, choice }) {
      const { variations } = state
      const index = variations.findIndex(v => v.id === id)
      const variation = Object.assign({}, variations[index], { choice })
      Vue.set(state.variations, index, variation)
    },
    removeTag (state, { id }) {
      const { variations } = state
      const index = variations.findIndex(v => v.id === id)
      const variation = Object.assign({}, variations[index], { choice: null })
      Vue.set(state.variations, index, variation)
    },
    clone (state, { id }) {
      const { variations } = state
      const index = variations.findIndex(v => v.id === id)
      const variation = Object.assign({}, variations[index], {
        id: variations.length,
        clone: true
      })
      Vue.set(state, 'variations', [...variations, variation])
    },
    removeClone (state, { id }) {
      const { variations } = state
      const index = variations.findIndex(v => v.id === id)
      const newVariations = variations.slice()
      newVariations.splice(index, 1)
      Vue.set(state, 'variations', newVariations)
    },
    setVariationText (state, { id, text }) {
      const { variations } = state
      const index = variations.findIndex(v => v.id === id)
      const variation = Object.assign({}, variations[index], { clause_found: text })
      console.log(variation)
      Vue.set(state.variations, index, variation)
    },
    setPath (state, path) {
      Vue.set(state, 'path', path)
    },
    setDiffField (state, field) {
      Vue.set(state, 'diffField', field);
    }
  },
  actions: {
    async bootstrap ({ commit, state }) {
      const { baseline, baselineMap, variations, categories } = await bootstrapDb()
      if (baseline !== null && variations !== null) {
        commit('bootstrap', { baseline, baselineMap, variations, categories })
      }
    },
    async clear ({ commit }) {
      await localforage.clear()
      commit('clear')
    },
    async setBaseline ({ commit, state }, baseline) {
      await localforage.setItem('baseline', baselineJson)
      const baselineMap = baseline.reduce((map, item) => {
        map[item.index] = item
        return map
      }, [])
      commit('setBaseline', { baseline, baselineMap })
      await localforage.setItem('baselineMap', baselineMap)
    },
    async setVariations ({ commit, state }, variations) {
      const variationsWithId = variations.map((v, idx) => Object.assign(v, { id: idx }))
      const variationsWithDiff = addDiffField(variationsWithId, state.baselineMap, state.diffField)
      await localforage.setItem('variations', variationsWithDiff)
      commit('setVariations', { variations: variationsWithDiff })
    },
    filter ({ commit, state }, { index }) {
      index.length > 0
        ? commit('setFilter', { index })
        : commit('removeFilter')
    },
    async addCategory ({ commit, state }, category) {
      commit('addCategory', { category })
      await localforage.setItem('categories', state.categories)
    },
    async editCategory ({ commit, state }, category) {
      commit('editCategory', { category })
      await localforage.setItem('categories', state.categories)
    },
    async tag ({ commit, state }, { id, choice }) {
      commit('setTag', { id, choice })
      await localforage.setItem('variations', state.variations)
    },
    async removeTag ({ commit, state }, id) {
      commit('removeTag', { id })
      await localforage.setItem('variations', state.variations)
    },
    async clone ({ commit, state }, id) {
      commit('clone', { id })
      await localforage.setItem('variations', state.variations)
    },
    async removeClone ({ commit, state }, id) {
      commit('removeClone', { id })
      await localforage.setItem('variations', state.variations)
    },
    changeVariationText ({ commit, state }, { id, text }) {
      commit('setVariationText', { id, text })
    },
    async changePath({ commit, state }, path) {
      commit('setPath', path)
      await localforage.setItem('path', state.path)
    },
    toggleDiffField({ commit, state }) {
      const fields = ["clean_text", "raw_text"];
      const currentIdx = fields.findIndex(field => field === state.diffField);
      const nextIdx = (currentIdx + 1 ) % 2;
      commit('setDiffField', fields[nextIdx]);
    }
  }
})

function getDiffHtml (a, b) {
  const diff = Diff.diffWordsWithSpace(a, b)
  let html = ''
  let color = ''
  diff.forEach(function (part) {
    color = part.added
      ? 'blue'
      : part.removed
        ? 'red'
        : 'grey'
    html += `<span class="${color}">${part.value}</span>`
  })

  const div = document.createElement('div')
  div.innerHTML = html
  const els = Array.from(div.children)
  const wrapper = document.createElement('div')
  let red = document.createDocumentFragment()
  let blue = document.createDocumentFragment()
  els.forEach(el => {
    if (el.className === 'grey') {
      if (el.textContent !== ' ') {
        red.childElementCount && wrapper.appendChild(red)
        blue.childElementCount && wrapper.appendChild(blue)
        wrapper.appendChild(el)
        red = document.createDocumentFragment()
        blue = document.createDocumentFragment()
      }
    } else if (el.className === 'red') {
      el.textContent = el.textContent + ' '
      red.appendChild(el)
    } else {
      el.textContent = el.textContent + ' '
      blue.appendChild(el)
    }
  })
  red.childElementCount && wrapper.appendChild(red)
  blue.childElementCount && wrapper.appendChild(blue)
  return wrapper.innerHTML
}

function addDiffField (variations, baselineMap, diffField) {
  return variations.map(variation => {
    return Object.assign(variation, {
      diff: getDiffHtml(
        baselineMap[variation.baseline_clause_index][diffField],
        variation.clause_found
      )
    })
  })
}
