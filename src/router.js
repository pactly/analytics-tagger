import Vue from 'vue'
import Router from 'vue-router'
import Tag from './views/Tag.vue'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'tag',
      component: Tag
    }
  ]
})
